# binutils for V16

This is a fork of [binutils-gdb](https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git) with support for [V16](https://gitlab.com/mbitsnbites/v16).

## Building

Prerequisites (Ubuntu):

```bash
$ sudo apt install build-essential texinfo
```

Configure and build:

```bash
$ cd binutils-v16
$ mkdir build
$ cd build
$ ../configure --target=v16-elf --with-system-zlib --disable-gdb --disable-sim
$ make
```

Install:

```bash
$ sudo make install
```

## About this Git repo

The V16 port of binutils is maintained as a branch that is periodically rebased on top of the latest upstream master branch and force pushed to the fork repository. To update your local clone you need to:

```bash
$ git fetch origin
$ git reset --hard origin/users/mbitsnbites/v16
```
