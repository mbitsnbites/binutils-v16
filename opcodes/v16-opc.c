/* Definitions for V16 opcodes.
   Copyright (C) 2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard (m@bitsnbites.eu).

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this file; see the file COPYING.  If not, write to the
   Free Software Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "opcode/v16.h"
#include "sysdep.h"

/* Helper macros for defining instructions.  */
#define V16OP_SPEC(insn, format, name, args, shift, signext)                   \
  {                                                                            \
    name, name "_" #args, insn, format, V16_ARGS_##args, shift, signext        \
  }

#define V16OP______(insn) V16OP_SPEC (insn, V16_FMT_BAD, "?", NONE, 0, false)

#define V16OP_FMT_A(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_A, name, args, shift, signext)
#define V16OP_FMT_B(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_B, name, args, shift, signext)
#define V16OP_FMT_C(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_C, name, args, shift, signext)
#define V16OP_FMT_D(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_D, name, args, shift, signext)
#define V16OP_FMT_E(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_E, name, args, shift, signext)
#define V16OP_FMT_F(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_F, name, args, shift, signext)
#define V16OP_FMT_G(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_G, name, args, shift, signext)
#define V16OP_FMT_H(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_H, name, args, shift, signext)
#define V16OP_FMT_I(insn, name, args, shift, signext)                          \
  V16OP_SPEC (insn, V16_FMT_I, name, args, shift, signext)

const v16_opc_info_t v16_opc_type_a_info[32] = {
    V16OP_FMT_A (0x0000, "nop", NONE, 0, false),
    V16OP_FMT_A (0x0010, "wait", NONE, 0, false),
    V16OP_FMT_A (0x0020, "vmaskf", NONE, 0, false),
    V16OP_FMT_A (0x0030, "vmaskt", NONE, 0, false),
    V16OP_FMT_A (0x0040, "anyvf", NONE, 0, false),
    V16OP_FMT_A (0x0050, "anyvt", NONE, 0, false),
    V16OP_FMT_A (0x0060, "allvf", NONE, 0, false),
    V16OP_FMT_A (0x0070, "allvt", NONE, 0, false),
    V16OP_FMT_A (0x0080, "spush", NONE, 0, false),
    V16OP_FMT_A (0x0090, "spop", NONE, 0, false),
    V16OP_FMT_A (0x00a0, "vclr", NONE, 0, false),
    V16OP_FMT_A (0x00b0, "reti", NONE, 0, false),
    V16OP______ (0x00c0),
    V16OP______ (0x00d0),
    V16OP______ (0x00e0),
    V16OP______ (0x00f0),

    V16OP_FMT_A (0x0100, "trap", I, 0, false),
    V16OP______ (0x0110), // (trap)
    V16OP______ (0x0120), // (trap)
    V16OP______ (0x0130), // (trap)
    V16OP______ (0x0140), // (trap)
    V16OP______ (0x0150), // (trap)
    V16OP______ (0x0160), // (trap)
    V16OP______ (0x0170), // (trap)
    V16OP______ (0x0180), // (trap)
    V16OP______ (0x0190), // (trap)
    V16OP______ (0x01a0), // (trap)
    V16OP______ (0x01b0), // (trap)
    V16OP______ (0x01c0), // (trap)
    V16OP______ (0x01d0), // (trap)
    V16OP______ (0x01e0), // (trap)
    V16OP______ (0x01f0), // (trap)
};

const v16_opc_info_t v16_opc_type_b_info[96] = {
    V16OP_FMT_B (0x0200, "j", R, 0, false),
    V16OP_FMT_B (0x0210, "jl", R, 0, false),
    V16OP_FMT_B (0x0220, "neg", R, 0, false),
    V16OP______ (0x0230),
    V16OP_FMT_B (0x0240, "extzb", R, 0, false),
    V16OP_FMT_B (0x0250, "extsb", R, 0, false),
    V16OP_FMT_B (0x0260, "extzh", R, 0, false),
    V16OP_FMT_B (0x0270, "extsh", R, 0, false),
    V16OP______ (0x0280),
    V16OP______ (0x0290),
    V16OP______ (0x02a0),
    V16OP______ (0x02b0),
    V16OP______ (0x02c0),
    V16OP______ (0x02d0),
    V16OP______ (0x02e0),
    V16OP______ (0x02f0),

    V16OP______ (0x0300),
    V16OP______ (0x0310),
    V16OP______ (0x0320),
    V16OP______ (0x0330),
    V16OP______ (0x0340),
    V16OP______ (0x0350),
    V16OP______ (0x0360),
    V16OP______ (0x0370),
    V16OP______ (0x0380),
    V16OP______ (0x0390),
    V16OP______ (0x03a0),
    V16OP______ (0x03b0),
    V16OP______ (0x03c0),
    V16OP______ (0x03d0),
    V16OP______ (0x03e0),
    V16OP______ (0x03f0),

    V16OP_FMT_B (0x0400, "sf", R, 0, false),
    V16OP_FMT_B (0x0410, "st", R, 0, false),
    V16OP______ (0x0420),
    V16OP______ (0x0430),
    V16OP______ (0x0440),
    V16OP______ (0x0450),
    V16OP______ (0x0460),
    V16OP______ (0x0470),
    V16OP______ (0x0480),
    V16OP______ (0x0490),
    V16OP______ (0x04a0),
    V16OP______ (0x04b0),
    V16OP______ (0x04c0),
    V16OP______ (0x04d0),
    V16OP______ (0x04e0),
    V16OP______ (0x04f0),

    V16OP______ (0x0500),
    V16OP______ (0x0510),
    V16OP______ (0x0520),
    V16OP______ (0x0530),
    V16OP______ (0x0540),
    V16OP______ (0x0550),
    V16OP______ (0x0560),
    V16OP______ (0x0570),
    V16OP______ (0x0580),
    V16OP______ (0x0590),
    V16OP______ (0x05a0),
    V16OP______ (0x05b0),
    V16OP______ (0x05c0),
    V16OP______ (0x05d0),
    V16OP______ (0x05e0),
    V16OP______ (0x05f0),

    V16OP_FMT_B (0x0600, "add1vl", R, 0, false),
    V16OP_FMT_B (0x0610, "add2vl", R, 0, false),
    V16OP_FMT_B (0x0620, "add4vl", R, 0, false),
    V16OP______ (0x0630),
    V16OP_FMT_B (0x0640, "sub1vl", R, 0, false),
    V16OP_FMT_B (0x0650, "sub2vl", R, 0, false),
    V16OP_FMT_B (0x0660, "sub4vl", R, 0, false),
    V16OP______ (0x0670),
    V16OP_FMT_B (0x0680, "setvl", R, 0, false),
    V16OP_FMT_B (0x0690, "nextvl", R, 0, false),
    V16OP______ (0x06a0),
    V16OP______ (0x06b0),
    V16OP______ (0x06c0),
    V16OP______ (0x06d0),
    V16OP______ (0x06e0),
    V16OP______ (0x06f0),

    V16OP_FMT_B (0x0700, "addf1", R, 0, false),
    V16OP_FMT_B (0x0710, "addf2", R, 0, false),
    V16OP_FMT_B (0x0720, "addf4", R, 0, false),
    V16OP______ (0x0730),
    V16OP______ (0x0740),
    V16OP______ (0x0750),
    V16OP______ (0x0760),
    V16OP______ (0x0770),
    V16OP_FMT_B (0x0780, "andf1", R, 0, false),
    V16OP_FMT_B (0x0790, "andf2", R, 0, false),
    V16OP_FMT_B (0x07a0, "andf4", R, 0, false),
    V16OP______ (0x07b0),
    V16OP_FMT_B (0x07c0, "orf1", R, 0, false),
    V16OP_FMT_B (0x07d0, "orf2", R, 0, false),
    V16OP_FMT_B (0x07e0, "orf4", R, 0, false),
    V16OP______ (0x07f0),
};

const v16_opc_info_t v16_opc_type_c_info[32] = {
    V16OP_FMT_C (0x0800, "vldb", V_R, 0, false),
    V16OP_FMT_C (0x0804, "vldh", V_R, 0, false),
    V16OP_FMT_C (0x0808, "vldw", V_R, 0, false),
    V16OP______ (0x080c),
    V16OP_FMT_C (0x0900, "vstb", V_R, 0, false),
    V16OP_FMT_C (0x0904, "vsth", V_R, 0, false),
    V16OP_FMT_C (0x0908, "vstw", V_R, 0, false),
    V16OP______ (0x090c),
    V16OP_FMT_C (0x0a00, "stride1", V_R, 0, false),
    V16OP_FMT_C (0x0a04, "stride2", V_R, 0, false),
    V16OP_FMT_C (0x0a08, "stride4", V_R, 0, false),
    V16OP______ (0x0a0c),
    V16OP______ (0x0b00),
    V16OP______ (0x0b04),
    V16OP______ (0x0b08),
    V16OP______ (0x0b0c),
    V16OP______ (0x0c00),
    V16OP______ (0x0c04),
    V16OP______ (0x0c08),
    V16OP______ (0x0c0c),
    V16OP______ (0x0d00),
    V16OP______ (0x0d04),
    V16OP______ (0x0d08),
    V16OP______ (0x0d0c),
    V16OP______ (0x0e00),
    V16OP______ (0x0e04),
    V16OP______ (0x0e08),
    V16OP______ (0x0e0c),
    V16OP______ (0x0f00),
    V16OP______ (0x0f04),
    V16OP______ (0x0f08),
    V16OP______ (0x0f0c),
};

const v16_opc_info_t v16_opc_type_d_info[32] = {
    V16OP_FMT_D (0x1000, "lsr", R_R, 0, false),
    V16OP_FMT_D (0x1100, "lsl", R_R, 0, false),
    V16OP_FMT_D (0x1200, "asr", R_R, 0, false),
    V16OP_FMT_D (0x1300, "cmpeq", R_R, 0, false),
    V16OP_FMT_D (0x1400, "cmplt", R_R, 0, false),
    V16OP_FMT_D (0x1500, "cmpltu", R_R, 0, false),
    V16OP_FMT_D (0x1600, "cmovf", R_R, 0, false),
    V16OP_FMT_D (0x1700, "cmovt", R_R, 0, false),
    V16OP_FMT_D (0x1800, "and", R_R, 0, false),
    V16OP_FMT_D (0x1900, "or", R_R, 0, false),
    V16OP_FMT_D (0x1a00, "xor", R_R, 0, false),
    V16OP_FMT_D (0x1b00, "mov", R_R, 0, false),
    V16OP_FMT_D (0x1c00, "add", R_R, 0, false),
    V16OP_FMT_D (0x1d00, "sub", R_R, 0, false),
    V16OP_FMT_D (0x1e00, "m2add", R_R, 0, false),
    V16OP_FMT_D (0x1f00, "m4add", R_R, 0, false),
    V16OP_FMT_D (0x2000, "mul", R_R, 0, false),
    V16OP_FMT_D (0x2100, "div", R_R, 0, false),
    V16OP_FMT_D (0x2200, "divu", R_R, 0, false),
    V16OP______ (0x2300),
    V16OP______ (0x2400),
    V16OP______ (0x2500),
    V16OP______ (0x2600),
    V16OP______ (0x2700),
    V16OP______ (0x2800),
    V16OP______ (0x2900),
    V16OP______ (0x2a00),
    V16OP______ (0x2b00),
    V16OP______ (0x2c00),
    V16OP______ (0x2d00),
    V16OP______ (0x2e00),
    V16OP______ (0x2f00),
};

const v16_opc_info_t v16_opc_type_e_info[12] = {
    V16OP_FMT_E (0x3000, "lsr", R_U, 0, false),
    V16OP_FMT_E (0x3100, "lsl", R_U, 0, false),
    V16OP_FMT_E (0x3200, "asr", R_U, 0, false),
    V16OP_FMT_E (0x3300, "cmpeq", R_I, 0, false),
    V16OP_FMT_E (0x3400, "cmplt", R_I, 0, false),
    V16OP_FMT_E (0x3500, "cmpltu", R_I, 0, false),
    V16OP_FMT_E (0x3600, "cmovf", R_I, 0, false),
    V16OP_FMT_E (0x3700, "cmovt", R_I, 0, false),
    V16OP_FMT_E (0x3800, "and", R_I, 0, false),
    V16OP_FMT_E (0x3900, "or", R_I, 0, false),
    V16OP_FMT_E (0x3a00, "xor", R_I, 0, false),
    V16OP______ (0x3b00),
};

const v16_opc_info_t v16_opc_type_f_info[4] = {
    V16OP_FMT_F (0x3c00, "bf", ADDR, 1, true),
    V16OP_FMT_F (0x3d00, "bt", ADDR, 1, true),
    V16OP_FMT_F (0x3e00, "b", ADDR, 1, true),
    V16OP_FMT_F (0x3f00, "bloop", ADDR, 1, true),
};

const v16_opc_info_t v16_opc_type_g_info[6] = {
    V16OP_FMT_G (0x4000, "ldb", R_R_I, 0, false),
    V16OP_FMT_G (0x5000, "stb", R_R_I, 0, false),
    V16OP_FMT_G (0x6000, "ldh", R_R_I, 1, false),
    V16OP_FMT_G (0x7000, "sth", R_R_I, 1, false),
    V16OP_FMT_G (0x8000, "ldw", R_R_I, 2, false),
    V16OP_FMT_G (0x9000, "stw", R_R_I, 2, false),
};

const v16_opc_info_t v16_opc_type_h_info[5] = {
    V16OP_FMT_H (0xa000, "mov", R_I, 0, true),
    V16OP_FMT_H (0xb000, "add", R_I, 0, true),
    V16OP_FMT_H (0xc000, "ldw", R_SP_I, 2, true),
    V16OP_FMT_H (0xd000, "stw", R_SP_I, 2, true),
    V16OP_FMT_H (0xe000, "ldw", R_ADDR, 2, true),
};

const v16_opc_info_t v16_opc_type_i_info[4] = {
    V16OP_FMT_I (0xf000, "lslins", R5_I, 0, false),
    V16OP_FMT_I (0xf400, "ldea", R5_ADDR, 2, false),
    V16OP_FMT_I (0xf800, "b", R5_ADDR, 2, false),
    V16OP_FMT_I (0xfc00, "bl", R5_ADDR, 2, false),
};
