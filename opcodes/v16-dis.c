/* Disassemble V16 instructions.
   Copyright (C) 2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard (m@bitsnbites.eu)

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#include "sysdep.h"
#include <assert.h>
#include <stdio.h>

#define STATIC_TABLE
#define DEFINE_TABLE

#include "disassemble.h"
#include "opcode/v16.h"

static const char *regs[16] = {"r1", "r2",  "r3", "r4", "r5", "r6", "r7", "r8",
			       "r9", "r10", "lr", "sp", "v1", "v2", "v3", "v4"};

/* Operation decoding for different formats.  */

static int
_get_op_a (const uint16_t iw)
{
  return (int) ((iw & 0x01f0u) >> 4);
}

static int
_get_op_b (const uint16_t iw)
{
  return ((int) ((iw & 0x0ff0u) >> 4)) - 32;
}

static int
_get_op_c (const uint16_t iw)
{
  const uint16_t op_hi = (iw & 0x0f00u) >> 6;
  const uint16_t op_lo = (iw & 0x000cu) >> 2;
  return ((int) (op_hi | op_lo)) - 32;
}

static int
_get_op_d (const uint16_t iw)
{
  return ((int) ((iw & 0xff00u) >> 8)) - 16;
}

static int
_get_op_e (const uint16_t iw)
{
  return ((int) ((iw & 0xff00u) >> 8)) - 48;
}

static int
_get_op_f (const uint16_t iw)
{
  return ((int) ((iw & 0xff00u) >> 8)) - 60;
}

static int
_get_op_g (const uint16_t iw)
{
  return ((int) ((iw & 0xf000u) >> 12)) - 4;
}

static int
_get_op_h (const uint16_t iw)
{
  return ((int) ((iw & 0xf000u) >> 12)) - 10;
}

static int
_get_op_i (const uint16_t iw)
{
  return ((int) ((iw & 0xfc00u) >> 10)) - 0x3c;
}

/* Register decoding.  */

static const char *
_get_rega (const uint16_t iw)
{
  return regs[iw & 0x000fu];
}

static const char *
_get_regb (const uint16_t iw)
{
  return regs[(iw & 0x00f0u) >> 4];
}

static const char *
_get_fmt_c_va (const uint16_t iw)
{
  return regs[(iw & 0x0003u) + 12];
}

/* Immediate value decoding.  */

static int
_get_imm_u4_4 (const uint16_t iw)
{
  return (int) ((iw & 0x00f0u) >> 4);
}

static int
_get_imm_s4_4 (const uint16_t iw)
{
  int i = _get_imm_u4_4 (iw);
  if ((i & 8) != 0)
    i |= 0xfffffff0;
  return i;
}

static int
_get_imm_u4_8 (const uint16_t iw)
{
  return (int) ((iw & 0x0f00u) >> 8);
}

static int
_get_imm_s8 (const uint16_t iw)
{
  return (int) ((int8_t) (iw & 0x00ffu));
}

static int
_get_imm_s8_4 (const uint16_t iw)
{
  return (int) ((int8_t) ((iw & 0x0ff0u) >> 4));
}

static int
_get_imm_u10 (const uint16_t iw)
{
  return (int) (iw & 0x03ffu);
}

static int
_get_imm_s10 (const uint16_t iw)
{
  if ((iw & 0x0200u) != 0u)
    {
      return (int) (int32_t) (0xfffffc00u | (uint32_t) (iw & 0x03ffu));
    }
  return (int) (iw & 0x03ffu);
}


/* Detect and decode compound calls (call, jump, ...). IWORD is the first
   instruction word. PC is the address of the first instruction. INFO holds
   disassble info. Returns the number extra bytes decoded (zero if this was
   not a compound sequence). */

static int
_handle_compound_calls (uint16_t iword, bfd_vma pc, struct disassemble_info *info)
{
  uint16_t iword_next;
  bfd_vma base_pc;
  int n;
  int offset;
  bfd_vma target;
  int lslins_count;
  const char* alias_name;
  const char* prefix;

  /* Is this a call/jump/addpc candidate (i.e. does it
     start with MOV R5,...)?  */
  if ((iword & 0xf00fu) != 0xa004u)
    return 0;

  offset = _get_imm_s8_4 (iword);
  base_pc = 0;

  /* Read up to three more instructions after the starting instruction.  */
  alias_name = NULL;
  lslins_count = 0;
  for (n = 1; n <= 3; ++n)
    {
      bfd_byte buffer[2];
      pc += 2;

      /* Make sure that we don't cross the end of a buffer.  */
      if ((info->buffer_length - (pc - info->buffer_vma) < 2)
	  || (info->stop_vma != 0 && pc > (info->stop_vma - 2)))
	return 0;

      /* Read the instruction.  */
      if (info->read_memory_func (pc, buffer, 2, info) != 0)
	return 0;
      iword_next = (uint16_t) bfd_getl16 (buffer);

      /* Decode the instruction.  */
      if ((iword_next & 0xfc00u) == 0xf000u)
	{
	  /* LSLINS R5, ... */
	  offset = (offset << 10) | _get_imm_u10 (iword_next);
	  ++lslins_count;
	}
      else if ((iword_next & 0xfc00u) == 0xf400u)
	{
	  /* LDEA R5, ... */
	  offset = (offset << 12) | (_get_imm_u10 (iword_next) << 2);
	  base_pc = pc;
	  alias_name = "addpc";
	  break;
	}
      else if ((iword_next & 0xfc00u) == 0xf800u)
	{
	  /* B R5, ... */
	  offset = (offset << 12) | (_get_imm_u10 (iword_next) << 2);
	  base_pc = pc;
	  alias_name = "jump";
	  break;
	}
      else if ((iword_next & 0xfc00u) == 0xfc00u)
	{
	  /* BL R5, ... */
	  offset = (offset << 12) | (_get_imm_u10 (iword_next) << 2);
	  base_pc = pc;
	  alias_name = "call";
	  break;
	}
      else
	{
	  /* This is not a supported compound sequence.  */
	  /* TODO(m): Do more stringent sequence checking.  */
	  return 0;
	}
    }

  if (alias_name == NULL)
    return 0;

  switch (lslins_count)
    {
    case 0:
      prefix = "";
      break;
    case 1:
      prefix = "l";
      break;
    case 2:
      prefix = "x";
      break;
    default:
      return 0;
    }

  /* Calculate the target address and print the instruction.  */
  target = (base_pc & ~3) + (bfd_vma) offset;
  info->fprintf_func (info->stream, "%s%s\t", prefix, alias_name);
  info->print_address_func ((bfd_vma) target, info);

  return 2 * (1 + n);
}

/* This is the main disassembler printing function.  */

int
print_insn_v16 (bfd_vma addr, struct disassemble_info *info)
{
  void *stream = info->stream;
  fprintf_ftype fpr = info->fprintf_func;
  bfd_byte buffer[2];
  int status;
  uint16_t iword;
  int length;
  int compound_length;
  int imm;
  const v16_opc_info_t *op;

  /* Configure the printer.  */
  info->bytes_per_chunk = 2;

  /* Read the 16-bit instruction word. Since this is a fixed width 16-bit
     ISA, that is all we need.  */
  if ((status = info->read_memory_func (addr, buffer, 2, info)))
    goto fail;
  iword = (uint16_t) bfd_getl16 (buffer);
  length = 2;

  /* Intelligently decode call, lcall, tail and ltail.  */
  compound_length = _handle_compound_calls (iword, addr, info);
  if (compound_length > 0)
    return compound_length;

  /* Decode and print the instruction.  */
  if (iword == 0x020au)
    {
      /* RET is in fact J lr  */
      fpr (stream, "ret");
    }
  else if ((iword & 0xff00u) == 0x0100u)
    {
      /* TRAP has a special 8-bit immediate encoding  */
      imm = (int) (iword & 0x00ffu);
      fpr (stream, "trap\t%d", imm);
    }
  else if ((iword & 0xfe00u) == 0x0000u)
    {
      /* Format A instruction  */
      op = &v16_opc_type_a_info[_get_op_a (iword)];
      fpr (stream, "%s", op->name);
    }
  else if ((iword & 0xf800u) == 0x0000u)
    {
      /* Format B instruction  */
      op = &v16_opc_type_b_info[_get_op_b (iword)];
      fpr (stream, "%s\t%s", op->name, _get_rega (iword));
    }
  else if ((iword & 0xf800u) == 0x0800u)
    {
      /* Format C instruction  */
      op = &v16_opc_type_c_info[_get_op_c (iword)];
      fpr (stream, "%s\t%s, %s", op->name, _get_fmt_c_va (iword),
	   _get_regb (iword));
    }
  else if ((iword & 0xf000u) <= 0x2000u)
    {
      /* Format D instruction  */
      op = &v16_opc_type_d_info[_get_op_d (iword)];
      fpr (stream, "%s\t%s, %s", op->name, _get_rega (iword),
	   _get_regb (iword));
    }
  else if ((iword & 0xfc00u) <= 0x3800u)
    {
      /* Format E instruction  */
      op = &v16_opc_type_e_info[_get_op_e (iword)];
      assert (!op->signext);
      if (op->args == V16_ARGS_R_U)
	imm = _get_imm_u4_4 (iword) << op->shift;
      else
	imm = _get_imm_s4_4 (iword) << op->shift;
      fpr (stream, "%s\t%s, %d", op->name, _get_rega (iword), imm);
    }
  else if ((iword & 0xfc00u) == 0x3c00u)
    {
      /* Format F instruction  */
      op = &v16_opc_type_f_info[_get_op_f (iword)];
      assert (op->signext);
      imm = _get_imm_s8 (iword) << op->shift;
      fpr (stream, "%s\t", op->name);
      info->print_address_func ((bfd_vma) (addr + imm), info);
    }
  else if ((iword & 0xf000u) <= 0x9000u)
    {
      /* Format G instruction  */
      op = &v16_opc_type_g_info[_get_op_g (iword)];
      assert (!op->signext);
      imm = _get_imm_u4_8 (iword) << op->shift;
      fpr (stream, "%s\t%s, [%s, %d]", op->name, _get_rega (iword),
	   _get_regb (iword), imm);
    }
  else if ((iword & 0xf000u) <= 0xe000u)
    {
      /* Format H instruction  */
      op = &v16_opc_type_h_info[_get_op_h (iword)];
      assert (op->signext);
      imm = _get_imm_s8_4 (iword) << op->shift;
      if (op->args == V16_ARGS_R_I)
	{
	  fpr (stream, "%s\t%s, %d", op->name, _get_rega (iword), imm);
	}
      else if (op->args == V16_ARGS_R_SP_I)
	{
	  fpr (stream, "%s\t%s, [sp, %d]", op->name, _get_rega (iword), imm);
	}
      else /* op->args == V16_ARGS_R_ADDR */
	{
	  fpr (stream, "%s\t%s, ", op->name, _get_rega (iword));
	  info->print_address_func ((bfd_vma) ((addr & ~3) + imm), info);
	}
    }
  else
    {
      /* Format I instruction  */
      op = &v16_opc_type_i_info[_get_op_i (iword)];
      imm = op->signext ? _get_imm_s10 (iword) : _get_imm_u10 (iword);
      imm <<= op->shift;
      fpr (stream, "%s\tr5, %d", op->name, imm);
    }

  return length;

fail:
  info->memory_error_func (status, addr, info);
  return -1;
}
