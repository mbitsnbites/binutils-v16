/* tc-v16.c -- V16 assembler
   Copyright (C) 2024 Free Software Foundation, Inc.

   Contributed by Marcus Geelnard (m@bitsnbites.eu).

   This file is part of GAS, the GNU Assembler.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GAS; see the file COPYING.  If not, write to
   the Free Software Foundation, 51 Franklin Street - Fifth Floor,
   Boston, MA 02110-1301, USA.  */

#include "as.h"
#include "safe-ctype.h"
#include "subsegs.h"
#include "tc-v16.h"
#include "opcode/v16.h"
#include "elf/v16.h"
#include "libiberty.h" /* ARRAY_SIZE()  */

/* Register numbers for special register aliases.
   The PC register is only accessible for certain instructions. We put it
   in a virtual register slot (> 15) during operand decode and resolve it
   later when we know what instruction we're dealing with.  */
#define V16_REG_R5 4
#define V16_REG_LR 10 /* R11  */
#define V16_REG_SP 11 /* R12  */
#define V16_REG_VPC 100

/* This array holds the chars that always start a comment.  If the
    pre-processor is disabled, these aren't very useful */
const char comment_chars[] = ";";

/* This array holds the chars that only start a comment at the beginning of
   a line.  If the line seems to have the form '# 123 filename'
   .line and .file directives will appear in the pre-processed output */
/* Note that input_file.c hand checks for '#' at the beginning of the
   first line of the input file.  This is because the compiler outputs
   #NO_APP at the beginning of its output.  */
/* Also note that C style comments are always supported.  */
const char line_comment_chars[] = ";#";

/* This array holds machine specific line separator characters.  */
const char line_separator_chars[] = "";

/* Chars that can be used to separate mant from exp in floating point nums */
const char EXP_CHARS[] = "eE";

/* Chars that mean this number is a floating point constant */
/* As in 0f12.456 */
/* or    0d1.2345e12 */
const char FLT_CHARS[] = "rRsSfFdDxXpP";

const pseudo_typeS md_pseudo_table[] = {{0, 0, 0}};

/* Target specific command line options.  */
enum options
{
  OPTION_RELAX = OPTION_MD_BASE,
  OPTION_NO_RELAX
};

const char md_shortopts[] = "";
const struct option md_longopts[] =
{
    {"mrelax", no_argument, NULL, OPTION_RELAX},
    {"mno-relax", no_argument, NULL, OPTION_NO_RELAX},
    {NULL, no_argument, NULL, 0}
};
const size_t md_longopts_size = sizeof (md_longopts);

/* Internal state.  */
static bfd_boolean s_v16_relax = TRUE;
static htab_t s_opc_map;

#define MAX_OP_STR_LEN 10
static char s_op_str[MAX_OP_STR_LEN + 1];

/* Literal pool structure. Held on a per-section and per-sub-section
   basis.  */

#define MAX_LITERAL_POOL_SIZE 128
typedef struct literal_pool_s
{
  expressionS literals[MAX_LITERAL_POOL_SIZE];
  unsigned int next_free_entry;
  unsigned int id;
  symbolS *symbol;
  segT section;
  subsegT sub_section;
#ifdef OBJ_ELF
  struct dwarf2_line_info locs[MAX_LITERAL_POOL_SIZE];
#endif
  struct literal_pool_s *next;
  unsigned int alignment;
} literal_pool_t;

/* Pointer to a linked list of literal pools.  */
static literal_pool_t *s_list_of_pools = NULL;

enum v16_operand_type
{
  V16_OT_BAD = 0,
  V16_OT_NONE,
  V16_OT_SREG,
  V16_OT_VREG,
  V16_OT_EXPRESSION
};

enum v16_modifier
{
  V16_MOD_NONE = 0,
  V16_MOD_PCHI,
  V16_MOD_PCMID,
  V16_MOD_PCLO
};

struct v16_operand_t
{
  enum v16_operand_type type;
  enum v16_modifier modifier;
  int reg_no;
  expressionS exp;
};

#define MAX_ARGS_ALTERNATIVES 4
typedef struct
{
  enum v16_args args[MAX_ARGS_ALTERNATIVES];
  int count;
} v16_args_alt_t;

static void
_clear_operand (struct v16_operand_t *operand)
{
  operand->type = V16_OT_NONE;
  operand->modifier = V16_MOD_NONE;
  operand->reg_no = -1;
  memset (&operand->exp, 0, sizeof (expressionS));
}

/* Get a literal pool for the current section and sub-section. If CREATE
   is TRUE, the literal pool will be created if one is not found.  */

static literal_pool_t *
_get_literal_pool (bfd_boolean create)
{
  /* Next literal pool ID number.  */
  static unsigned int latest_pool_num = 1;
  literal_pool_t *pool;

  /* Find the literal pool for the current section and sub-section
     (or NULL).  */
  for (pool = s_list_of_pools; pool != NULL; pool = pool->next)
    {
      if (pool->section == now_seg && pool->sub_section == now_subseg)
	break;
    }

  /* Create a new literal pool if necessary.  */
  if (pool == NULL)
    {
      if (!create)
	return NULL;

      /* Create a new pool.  */
      pool = XNEW (literal_pool_t);
      if (!pool)
	{
	  as_bad (_ ("no memory for a new literal pool"));
	  return NULL;
	}

      pool->next_free_entry = 0;
      pool->section = now_seg;
      pool->sub_section = now_subseg;
      pool->next = s_list_of_pools;
      pool->symbol = NULL;
      pool->alignment = 2;

      /* Add it to the list.  */
      s_list_of_pools = pool;
    }

  /* New pools, and emptied pools, will have a NULL symbol.  */
  if (pool->symbol == NULL)
    {
      pool->symbol = symbol_create (FAKE_LABEL_NAME, undefined_section,
				    &zero_address_frag, 0);
      pool->id = latest_pool_num++;
    }

  /* Done.  */
  return pool;
}

/* Store the given operand in the literal pool and adjust OP to represent the
   address of the literal pool entry (i.e. suitable for use with LDW).  */

static bfd_boolean
_add_to_literal_pool (struct v16_operand_t *op)
{
  literal_pool_t *pool;
  unsigned int entry;
  unsigned int entry_offset;

  pool = _get_literal_pool (TRUE);
  if (pool == NULL)
    return FALSE;

  /* Check if this literal value is already in the pool.  */
  entry_offset = 0;
  for (entry = 0; entry < pool->next_free_entry; entry++)
    {
      if ((pool->literals[entry].X_op == op->exp.X_op) &&
	  (op->exp.X_op == O_constant) &&
	  (pool->literals[entry].X_add_number == op->exp.X_add_number) &&
	  (pool->literals[entry].X_unsigned == op->exp.X_unsigned))
	break;

      if ((pool->literals[entry].X_op == op->exp.X_op) &&
	  (op->exp.X_op == O_symbol) &&
	  (pool->literals[entry].X_add_number == op->exp.X_add_number) &&
	  (pool->literals[entry].X_add_symbol == op->exp.X_add_symbol) &&
	  (pool->literals[entry].X_op_symbol == op->exp.X_op_symbol))
	break;

      entry_offset += 4;
    }

  /* Do we need to create a new entry?	*/
  if (entry == pool->next_free_entry)
    {
      if (entry >= MAX_LITERAL_POOL_SIZE)
	{
	  as_bad (_ ("literal pool overflow"));
	  return FALSE;
	}

      /* Store the exp in the literal pool.  */
      pool->literals[entry] = op->exp;

#ifdef OBJ_ELF
      /* PR ld/12974: Record the location of the first source line to reference
	 this entry in the literal pool.  If it turns out during linking that
	 the symbol does not exist we will be able to give an accurate line
	 number for the (first use of the) missing reference.  */
      if (debug_type == DEBUG_DWARF2)
	dwarf2_where (pool->locs + entry);
#endif
      pool->next_free_entry += 1;
    }

  /* Let the operand point the literal pool entry.  */
  op->exp.X_op = O_symbol;
  op->exp.X_add_symbol = pool->symbol;
  op->exp.X_add_number = entry_offset;

  return TRUE;
}

static const char *
_v16_args_to_string (enum v16_args args)
{
  switch (args)
    {
    case V16_ARGS_NONE:
      return "NONE";
    case V16_ARGS_I:
      return "I";
    case V16_ARGS_R:
      return "R";
    case V16_ARGS_V_R:
      return "V_R";
    case V16_ARGS_R_R:
      return "R_R";
    case V16_ARGS_R_I:
      return "R_I";
    case V16_ARGS_R_U:
      return "R_U";
    case V16_ARGS_ADDR:
      return "ADDR";
    case V16_ARGS_R_R_I:
      return "R_R_I";
    case V16_ARGS_R_SP_I:
      return "R_SP_I";
    case V16_ARGS_R_ADDR:
      return "R_ADDR";
    case V16_ARGS_R5_I:
      return "R5_I";
    case V16_ARGS_R5_ADDR:
      return "R5_ADDR";
    default:
      as_bad (_ ("unrecognized argument specifier"));
      return "?";
    }
}

/* This function creates a opcode key in the same way as the
   V16OP_SPEC macro in v16-opc.c. Be sure to keep the two in
   sync!  */

static const char *
_make_opc_map_key (const char *name, enum v16_args args)
{
  static char s_key[100];
  const char *args_str = _v16_args_to_string (args);
  const int n = snprintf (s_key, ARRAY_SIZE (s_key), "%s_%s", name, args_str);
  if (n < 1 || (size_t) n >= ARRAY_SIZE (s_key))
    {
      /* An error occurred (e.g. name was too long).
	 Return an empty string.  */
      s_key[0] = 0;
    }
  return s_key;
}

static int
_get_sreg_no (const char *name, const int len)
{
  if (len == 2)
    {
      if (name[0] == 'p' && name[1] == 'c')
	return V16_REG_VPC;
      if (name[0] == 'l' && name[1] == 'r')
	return V16_REG_LR;
      if (name[0] == 's' && name[1] == 'p')
	return V16_REG_SP;
      if (name[0] == 'r')
	{
	  const int reg_no = name[1] - '0';
	  if (reg_no >= 1 && reg_no <= 9)
	    return reg_no - 1;
	}
    }
  else if (len == 3)
    {
      if (name[0] == 'r')
	{
	  const int reg_no_hi = name[1] - '0';
	  const int reg_no_lo = name[2] - '0';
	  if (reg_no_hi >= 1 && reg_no_hi <= 3 && reg_no_lo >= 0 &&
	      reg_no_lo <= 9)
	    {
	      const int reg_no = (10 * reg_no_hi) + reg_no_lo;
	      if (reg_no >= 1 && reg_no <= 12)
		return reg_no - 1;
	    }
	}
    }

  return -1;
}

static int
_get_vreg_no (const char *name, const int len)
{
  if (len == 2)
    {
      if (name[0] == 'v')
	{
	  const int reg_no = name[1] - '0';
	  if (reg_no >= 1 && reg_no <= 4)
	    return reg_no + 11;
	}
    }

  return -1;
}

static bfd_boolean
_is_reg (const struct v16_operand_t *operand)
{
  return operand->type == V16_OT_SREG || operand->type == V16_OT_VREG;
}

static bfd_boolean
_is_imm (const struct v16_operand_t *operand)
{
  return operand->type == V16_OT_EXPRESSION;
}

static v16_args_alt_t
_operands_to_args (struct v16_operand_t *op1, struct v16_operand_t *op2,
		   struct v16_operand_t *op3)
{
  v16_args_alt_t args_alt = {};

  /* Note: The order of the alternatives matter: best alternatives first.  */

  if (op1->type == V16_OT_NONE && op2->type == V16_OT_NONE &&
      op3->type == V16_OT_NONE)
    {
      args_alt.args[args_alt.count++] = V16_ARGS_NONE;
      return args_alt;
    }

  if (_is_imm (op1) && op2->type == V16_OT_NONE && op3->type == V16_OT_NONE)
    {
      args_alt.args[args_alt.count++] = V16_ARGS_I;
      args_alt.args[args_alt.count++] = V16_ARGS_ADDR;
      return args_alt;
    }

  if (_is_reg (op1) && op2->type == V16_OT_NONE && op3->type == V16_OT_NONE)
    {
      args_alt.args[args_alt.count++] = V16_ARGS_R;
      return args_alt;
    }

  if (_is_reg (op1) && _is_reg (op2) && op3->type == V16_OT_NONE)
    {
      if (op1->type == V16_OT_VREG)
	{
	  args_alt.args[args_alt.count++] = V16_ARGS_V_R;
	}
      args_alt.args[args_alt.count++] = V16_ARGS_R_R;
      return args_alt;
    }

  if (_is_reg (op1) && _is_imm (op2) && op3->type == V16_OT_NONE)
    {
      if (op1->reg_no == V16_REG_R5)
	{
	  args_alt.args[args_alt.count++] = V16_ARGS_R5_I;
	  args_alt.args[args_alt.count++] = V16_ARGS_R5_ADDR;
	}
      args_alt.args[args_alt.count++] = V16_ARGS_R_I;
      args_alt.args[args_alt.count++] = V16_ARGS_R_U;
      args_alt.args[args_alt.count++] = V16_ARGS_R_ADDR;
      return args_alt;
    }

  if (_is_reg (op1) && _is_reg (op2) && _is_imm (op3))
    {
      if (op2->reg_no == V16_REG_SP)
	args_alt.args[args_alt.count++] = V16_ARGS_R_SP_I;
      args_alt.args[args_alt.count++] = V16_ARGS_R_R_I;
      return args_alt;
    }

  as_bad (_ ("unsupported operands"));
  return args_alt;
}

static char *
_decode_expression (char *str, expressionS *exp)
{
  char *str_end;
  char *save;

  /* Use expression() to decode the zero-terminated string STR.  */
  save = input_line_pointer;
  input_line_pointer = str;
  expression (exp);
  str_end = input_line_pointer;
  input_line_pointer = save;

  return str_end;
}

static char *
_decode_operand (char *str, struct v16_operand_t *operand)
{
  char *str_end;
  char *mod;
  int str_len;
  int reg_no;

  /* Clear the operand info.  */
  _clear_operand (operand);

  /* Define the string of the operand (and find the end of the operand
     string).  */
  for (str_end = str; !is_end_of_line[(unsigned char) *str_end] &&
		      !ISSPACE (*str_end) && *str_end != ',';
       ++str_end)
    ;
  str_len = (int) (str_end - str);

  /* Decode the operand type.  */
  if (str_len > 0)
    {
      if ((reg_no = _get_sreg_no (str, str_len)) >= 0)
	{
	  operand->type = V16_OT_SREG;
	  operand->reg_no = reg_no;
	}
      else if ((reg_no = _get_vreg_no (str, str_len)) >= 0)
	{
	  operand->type = V16_OT_VREG;
	  operand->reg_no = reg_no;
	}
      else
	{
	  /* Drop the optional # prefix.  */
	  if (*str == '#')
	    ++str;

	  /* Find the optional operand modifier '@'.  */
	  for (mod = str; mod < str_end && *mod != '@'; ++mod)
	    ;
	  if (mod < str_end)
	    {
	      /* Decode operand modifiers: @pc.  */
	      /* TODO(m): Make sure that there are no trailing chars.  */
	      if (strncmp (mod, "@pchi", 5) == 0)
		operand->modifier = V16_MOD_PCHI;
	      else if (strncmp (mod, "@pcmid", 6) == 0)
		operand->modifier = V16_MOD_PCMID;
	      else if (strncmp (mod, "@pclo", 5) == 0)
		operand->modifier = V16_MOD_PCLO;

	      /* Temporarily terminate the string at the @ (so that it is not
		 included in _decode_expression).  */
	      *mod = 0;
	    }

	  /* Decode the expression.  */
	  (void) _decode_expression (str, &operand->exp);

	  /* Restore the @ in the string, if necessary.  */
	  if (mod < str_end)
	    *mod = '@';

	  if (operand->exp.X_op != O_illegal && operand->exp.X_op != O_absent)
	    {
	      operand->type = V16_OT_EXPRESSION;
	    }
	  else
	    {
	      as_bad (_ ("invalid operand"));
	      operand->type = V16_OT_BAD;
	    }
	}
    }

  /* Find the start of the next operand.  */
  for (; !is_end_of_line[(unsigned char) *str_end] &&
	 (ISSPACE (*str_end) || *str_end == ',');
       ++str_end)
    ;
  return str_end;
}

static char *
_decode_address_operands (char *str, struct v16_operand_t *operand1,
			  struct v16_operand_t *operand2)
{
  char *str_end;
  char *operands_end;

  /* Find the start and length of address specification.  */
  if (!is_end_of_line[(unsigned char) *str] && *str == '[')
    ++str;
  for (str_end = str;
       !is_end_of_line[(unsigned char) *str_end] && *str_end != ']'; ++str_end)
    ;
  if (*str_end != ']')
    {
      as_bad (_ ("invalid address specifier"));
      _clear_operand (operand1);
      _clear_operand (operand2);
      return str_end;
    }

  /* Temporarily terminate the string at the ending ] character.  */
  *str_end = 0;

  /* Decode up to two operands.  */
  operands_end = _decode_operand (str, operand1);
  operands_end = _decode_operand (operands_end, operand2);

  /* Check that we consumed all the operands.  */
  if (operands_end != str_end)
    as_warn (_ ("trailing address operand extras ignored: %s"), operands_end);

  /* Restore the ] character.  */
  *str_end = ']';

  /* Handle implicit operands.  */
  if (_is_reg (operand1) && operand2->type == V16_OT_NONE)
    {
      /* Tranlaste "[reg]" to "[reg, #0]".  */
      char operand_str[3] = "#0";
      _decode_operand (&operand_str[0], operand2);
    }

  /* Find the start of the next operand.  */
  str_end++;
  for (; !is_end_of_line[(unsigned char) *str_end] &&
	 (ISSPACE (*str_end) || *str_end == ',');
       ++str_end)
    ;
  return str_end;
}

static bfd_boolean
_resolve_immediate (const struct v16_operand_t *op,
		    const v16_opc_info_t *opcode, int min_value, int max_value,
		    int *imm)
{
  int value;
  int mult;

  /* Is this a constant (we don't support relocs in immediate fields)?  */
  if (op->exp.X_op != O_constant)
    {
      as_bad (_ ("not a constant"));
      *imm = 0;
      return FALSE;
    }
  value = (int) op->exp.X_add_number;

  /* Adjust the immediate according to the shift amount.  */
  mult = 1 << opcode->shift;
  if ((value & (mult - 1)) != 0)
    {
      as_bad (_ ("immediate operand not a multiple of %d"), mult);
      *imm = 0;
      return FALSE;
    }
  value >>= opcode->shift;

  /* Check the range. */
  if (value < min_value || value > max_value)
    {
      as_bad (_ ("immediate operand out of range [%d,%d]"), min_value,
	      max_value);
      return FALSE;
    }

  *imm = value;
  return TRUE;
}

static const char *
_translate_alias (char *op_start, char *op_end, struct v16_operand_t *op1,
		  struct v16_operand_t *op2, struct v16_operand_t *op3)
{
  /* Copy the op string to s_op_str.  */
  const int str_len = (int) (op_end - op_start);
  if (str_len > MAX_OP_STR_LEN)
    {
      as_bad (_ ("unsupported op code"));
      s_op_str[0] = 0;
      return &s_op_str[0];
    }
  memcpy (&s_op_str[0], op_start, (size_t) str_len);
  s_op_str[str_len] = 0;

  /* Convert known aliases to their canonical representation.  */
  if (str_len == 3 && (memcmp (&s_op_str[0], "ret", str_len) == 0) &&
      (op1->type == V16_OT_NONE) && (op2->type == V16_OT_NONE) &&
      (op3->type == V16_OT_NONE))
    {
      /* Tranlaste "ret" to "j lr".  */
      strcpy (&s_op_str[0], "j");
      _clear_operand (op1);
      op1->type = V16_OT_SREG;
      op1->reg_no = V16_REG_LR;
    }

  return &s_op_str[0];
}

static void
_emit_instruction (const char *op_str, struct v16_operand_t *op1,
		   struct v16_operand_t *op2, struct v16_operand_t *op3)
{
  struct v16_operand_t *reloc_operand;
  const v16_opc_info_t *opcode;
  uint16_t iword;
  int imm;

  int pc_rel;
  unsigned long where;
  bfd_reloc_code_real_type reloc_type;

  /* Look up the matching op code for the different possible argument
     candidates.  */
  opcode = NULL;
  v16_args_alt_t args_alt = _operands_to_args (op1, op2, op3);
  for (int i = 0; i < args_alt.count; ++i)
    {
      const char *opc_key = _make_opc_map_key (op_str, args_alt.args[i]);
      opcode = (v16_opc_info_t *) str_hash_find (s_opc_map, opc_key);
      if (opcode != NULL)
	break;
    }
  if (opcode == NULL)
    {
      as_bad (_ ("unknown opcode %s"), op_str);
      return;
    }

  /* Reserve space for the instruction (it's always 2 bytes = 16 bits).  */
  char *p = frag_more (2);

  /* Default: No reloc.  */
  pc_rel = 0;
  where = 0;
  reloc_type = BFD_RELOC_NONE;

  /* Constuct the instruction word.  */
  iword = opcode->insn;
  switch (opcode->format)
    {
    case V16_FMT_A:
      {
	if (opcode->args == V16_ARGS_I)
	  {
	    if (!_resolve_immediate (op1, opcode, 0, 255, &imm))
	      return;
	    iword |= (uint16_t) imm;
	  }
	else
	  {
	    gas_assert (opcode->args == V16_ARGS_NONE);
	  }

	break;
      }

    case V16_FMT_B:
      {
	gas_assert (opcode->args == V16_ARGS_R);

	iword |= (uint16_t) op1->reg_no;
	break;
      }

    case V16_FMT_C:
      {
	gas_assert (opcode->args == V16_ARGS_V_R);

	if (op1->reg_no < 12)
	  {
	    as_bad (_ ("unsupported register operands"));
	    return;
	  }
	iword |= (uint16_t) (op1->reg_no - 12);
	iword |= ((uint16_t) op2->reg_no) << 4;
	break;
      }

    case V16_FMT_D:
      {
	gas_assert (opcode->args == V16_ARGS_R_R);

	iword |= (uint16_t) op1->reg_no;
	iword |= ((uint16_t) op2->reg_no) << 4;
	break;
      }

    case V16_FMT_E:
      {
	gas_assert (opcode->args == V16_ARGS_R_I
		    || opcode->args == V16_ARGS_R_U);

	/* Unsigned or signed 4-bit immediate value?  */
	int min_value = (opcode->args == V16_ARGS_R_U) ? 0: -8;
	int max_value = (opcode->args == V16_ARGS_R_U) ? 15 : 7;

	if (!_resolve_immediate (op2, opcode, min_value, max_value, &imm))
	  return;
	iword |= (uint16_t) op1->reg_no;
	iword |= ((uint16_t) (imm & 15U)) << 4;
	break;
      }

    case V16_FMT_F:
      {
	gas_assert (opcode->args == V16_ARGS_ADDR);

	/* PC-relative reloc.  */
	gas_assert (opcode->shift == 1);
	reloc_operand = op1;
	where = (unsigned long) (p - frag_now->fr_literal);
	reloc_type = BFD_RELOC_V16_PCREL_8X2;
	pc_rel = 1;
	break;
      }

    case V16_FMT_G:
      {
	gas_assert (opcode->args == V16_ARGS_R_R_I);

	if (!_resolve_immediate (op3, opcode, 0, 15, &imm))
	  return;
	iword |= (uint16_t) op1->reg_no;
	iword |= ((uint16_t) op2->reg_no) << 4;
	iword |= ((uint16_t) imm) << 8;
	break;
      }

    case V16_FMT_H:
      {
	iword |= (uint16_t) op1->reg_no;

	/* Handle the immediate operand.  */
	if (opcode->args == V16_ARGS_R_ADDR)
	  {
	    /* PC-relative reloc.  */
	    gas_assert (opcode->shift == 2);
	    reloc_operand = op2;
	    where = (unsigned long) (p - frag_now->fr_literal);
	    reloc_type = BFD_RELOC_V16_PCREL_8X4_SHL4;
	    pc_rel = 1;
	  }
	else if (opcode->args == V16_ARGS_R_SP_I)
	  {
	    if (!_resolve_immediate (op3, opcode, -128, 127, &imm))
	      return;
	    iword |= (((uint16_t) imm) & 0xffu) << 4;
	  }
	else
	  {
	    gas_assert (opcode->args == V16_ARGS_R_I);

	    if (op2->modifier == V16_MOD_NONE)
	      {
		/* A constant immediate value.  */
		if (!_resolve_immediate (op2, opcode, -128, 127, &imm))
		  return;
		iword |= (((uint16_t) imm) & 0xffu) << 4;
	      }
	    else
	      {
		/* PC-relative reloc.  */
		if (op2->modifier == V16_MOD_PCHI)
		  reloc_type = BFD_RELOC_V16_PCREL_HI8_SHL4;
		else
		  {
		    gas_assert (op2->modifier == V16_MOD_PCMID);
		    reloc_type = BFD_RELOC_V16_PCREL_MID8_SHL4;
		  }
		reloc_operand = op2;
		where = (unsigned long) (p - frag_now->fr_literal);
		pc_rel = 1;
	      }
	  }
	break;
      }

    case V16_FMT_I:
      {
	/* Handle the immediate operand.  */
	if (opcode->args == V16_ARGS_R5_I)
	  {
	    if (op2->modifier == V16_MOD_NONE)
	      {
		/* A constant immediate value.  */
		const int lo = opcode->signext ? -512 : 0;
		const int hi = opcode->signext ? 511 : 1023;
		if (!_resolve_immediate (op2, opcode, lo, hi, &imm))
		  return;
		iword |= (((uint16_t) imm) & 0x3ffu);
	      }
	    else
	      {
		/* PC-relative reloc.  */
		if (op2->modifier == V16_MOD_PCHI)
		  reloc_type = BFD_RELOC_V16_PCREL_HI10;
		else
		  {
		    gas_assert (op2->modifier == V16_MOD_PCMID);
		    reloc_type = BFD_RELOC_V16_PCREL_MID10;
		  }
		reloc_operand = op2;
		where = (unsigned long) (p - frag_now->fr_literal);
		pc_rel = 1;
	      }
	  }
	else
	  {
	    gas_assert (opcode->args == V16_ARGS_R5_ADDR);

	    /* PC-relative reloc.  */
	    gas_assert (op2->modifier == V16_MOD_NONE ||
			op2->modifier == V16_MOD_PCLO);
	    reloc_type = BFD_RELOC_V16_PCREL_LO10X4;
	    reloc_operand = op2;
	    where = (unsigned long) (p - frag_now->fr_literal);
	    pc_rel = 1;
	  }
	break;
      }

    default:
      abort ();
      break;
    }

  /* Add a reloc?  */
  if (reloc_type != BFD_RELOC_NONE)
    {
      fixS *fixP;

      gas_assert (reloc_operand != NULL);
      reloc_howto_type *howto = bfd_reloc_type_lookup (stdoutput, reloc_type);
      if (howto == NULL)
	as_bad (_ ("Unsupported V16 relocation number %d"), reloc_type);
      fixP = fix_new_exp (frag_now, where, bfd_get_reloc_size (howto),
			  &reloc_operand->exp, pc_rel, reloc_type);

      /* Turn off complaints that some relocs do not fit in 2 bytes. */
      switch (reloc_type)
	{
	case BFD_RELOC_V16_PCREL_HI8_SHL4:
	case BFD_RELOC_V16_PCREL_MID8_SHL4:
	case BFD_RELOC_V16_PCREL_HI10:
	case BFD_RELOC_V16_PCREL_MID10:
	case BFD_RELOC_V16_PCREL_LO10X4:
	  fixP->fx_no_overflow = 1;
	  break;
	default:
	  break;
	}
    }

  /* Emit the instruction (always two bytes).  */
  md_number_to_chars (p, iword, 2);
}

static bfd_boolean
_expand_alias (const char *op_str, struct v16_operand_t *op1,
	       struct v16_operand_t *op2, struct v16_operand_t *op3)
{
  /* Expand "ldi" to suitable instruction combinations.  */
  if (strcmp (op_str, "ldi") == 0)
    {
      if (!_is_reg (op1) || op2->type != V16_OT_EXPRESSION ||
	  op3->type != V16_OT_NONE)
	{
	  as_bad (_ ("invalid operands"));
	  return TRUE;
	}

      /* Can we use MOV?  */
      if (op2->exp.X_op == O_constant)
	{
	  int value = (int) op2->exp.X_add_number;
	  if (value >= -128 && value <= 127)
	    {
	      _emit_instruction ("mov", op1, op2, op3);
	      return TRUE;
	    }
	  else if (op1->reg_no == V16_REG_R5 && value >= -131072 && value <= 131071)
	    {
	      /* Move upper 8 bits to register.  */
	      op2->exp.X_add_number = value >> 10;
	      _emit_instruction ("mov", op1, op2, op3);

	      /* Shift and insert lower 10 bits into register.  */
	      op2->exp.X_add_number = value & 0x3ff;
	      _emit_instruction ("lslins", op1, op2, op3);
	      return TRUE;
	    }
	  else if (op1->reg_no == V16_REG_R5 && value >= -134217728 && value <= 134217727)
	    {
	      /* Move upper 8 bits to register.  */
	      op2->exp.X_add_number = value >> 20;
	      _emit_instruction ("mov", op1, op2, op3);

	      /* Shift and insert mid 10 bits into register.  */
	      op2->exp.X_add_number = (value >> 10) & 0x3ff;
	      _emit_instruction ("lslins", op1, op2, op3);

	      /* Shift and insert lower 10 bits into register.  */
	      op2->exp.X_add_number = value & 0x3ff;
	      _emit_instruction ("lslins", op1, op2, op3);
	      return TRUE;
	    }
	}

      /* TODO(m): Support MOV R5 + LSLINS R5 + LDEA R5...?  */

      /* Revert to using the literal pool (both large constants and relocs).  */
      if (!_add_to_literal_pool (op2))
	return TRUE;
      _emit_instruction ("ldw", op1, op2, op3);

      return TRUE;
    }

  /* Expand "call", "lcall", "jump" and "ljump" to suitable instruction
     combinations.  */
  if (strcmp (op_str, "call") == 0 || strcmp (op_str, "lcall") == 0 ||
      strcmp (op_str, "jump") == 0 || strcmp (op_str, "ljump") == 0)
    {
      offsetT add_number;

      if (op1->type != V16_OT_EXPRESSION || op2->type != V16_OT_NONE ||
	  op3->type != V16_OT_NONE)
	{
	  as_bad (_ ("invalid operands"));
	  return TRUE;
	}

      add_number = op1->exp.X_add_number;

      /* Change operands.  */
      *op2 = *op1;
      _clear_operand (op1);
      op1->type = V16_OT_SREG;
      op1->reg_no = V16_REG_R5;

      /* Is this a long call ("lcall"/"ljump")?  */
      if (op_str[0] == 'l')
	{
	  /* mov r5, addr@pchi */
	  op2->modifier = V16_MOD_PCHI;
	  op2->exp.X_add_number = add_number - 4;
	  _emit_instruction ("mov", op1, op2, op3);

	  /* lslins r5, addr@pcmid */
	  op2->modifier = V16_MOD_PCMID;
	  op2->exp.X_add_number = add_number - 2;
	  _emit_instruction ("lslins", op1, op2, op3);
	}
      else
	{
	  /* mov r5, addr@pcmid */
	  op2->modifier = V16_MOD_PCMID;
	  op2->exp.X_add_number = add_number - 2;
	  _emit_instruction ("mov", op1, op2, op3);
	}

      /* b(l) r5, addr@pclo */
      op2->modifier = V16_MOD_PCLO;
      op2->exp.X_add_number = add_number;
      if (strcmp (op_str, "call") == 0 || strcmp (op_str, "lcall") == 0)
	{
	  _emit_instruction ("bl", op1, op2, op3);
	}
      else
	{
	  _emit_instruction ("b", op1, op2, op3);
	}

      return TRUE;
    }

  /* The instruction was not expanded.  */
  return FALSE;
}

void
md_operand (expressionS *op ATTRIBUTE_UNUSED)
{
  /* Empty for now. */
}

/* This function is called once, at assembler startup time.  It sets
   up the hash table with all the opcodes in it, and also initializes
   some aliases for compatibility with other assemblers.  */
void
md_begin (void)
{
  size_t k;

  /* Create a hash table for looking up instructions.  */
  s_opc_map = str_htab_create ();

  /* Insert format A opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_a_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_a_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format B opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_b_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_b_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format C opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_c_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_c_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format D opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_d_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_d_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format E opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_e_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_e_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format F opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_f_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_f_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format G opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_g_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_g_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format H opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_h_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_h_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  /* Insert format I opcodes into the hash table.  */
  for (k = 0; k < ARRAY_SIZE (v16_opc_type_i_info); ++k)
    {
      const v16_opc_info_t *opcode = &v16_opc_type_i_info[k];
      if (opcode->format != V16_FMT_BAD)
	str_hash_insert (s_opc_map, opcode->key, (void *) opcode, 0);
    }

  bfd_set_arch_mach (stdoutput, TARGET_ARCH, 0);
}

/* This is the guts of the machine-dependent assembler.  STR points to
   a machine dependent instruction.  This function is supposed to emit
   the frags/bytes it assembles to.  */
void
md_assemble (char *str)
{
  char *op_start;
  char *op_end;
  const char *op_str;
  char *operand_start;
  char *operands_end;

  struct v16_operand_t operand1;
  struct v16_operand_t operand2;
  struct v16_operand_t operand3;

  /* Drop leading whitespace.  */
  while (ISSPACE (*str))
    ++str;

  /* Find the op code end.  */
  op_start = str;
  for (op_end = op_start;
       !is_end_of_line[(unsigned char) *op_end] && !ISSPACE (*op_end) &&
       (*op_end != '.') && (*op_end != '/') && (*op_end != '_');
       ++op_end)
    ;
  if (op_end == op_start)
    {
      as_bad (_ ("can't find opcode"));
      return;
    }

  /* Find the start of the operands.  */
  for (operand_start = op_end;
       !is_end_of_line[(unsigned char) *operand_start] &&
       ISSPACE (*operand_start);
       ++operand_start)
    ;

  /* Decode the operands (there are max three operands).  */
  operand_start = _decode_operand (operand_start, &operand1);
  if (*operand_start == '[')
    {
      operands_end =
	  _decode_address_operands (operand_start, &operand2, &operand3);
    }
  else
    {
      operand_start = _decode_operand (operand_start, &operand2);
      operands_end = _decode_operand (operand_start, &operand3);
    }
  if (operand1.type == V16_OT_BAD || operand2.type == V16_OT_BAD ||
      operand3.type == V16_OT_BAD)
    return;

  /* Check the end of the line (mostly to warn about trailing garbage).  */
  while (ISSPACE (*operands_end))
    ++operands_end;
  if (*operands_end != 0)
    as_warn (_ ("extra stuff on line ignored"));

  /* Handle aliases.  After this call, op_str contains the correct op string,
     and operand1, operand2 and operand3 may have been adjusted.  */
  op_str = _translate_alias (op_start, op_end, &operand1, &operand2, &operand3);

  /* Some aliases expand to more than one instruction. If the instruction gets
     expanded, we're done. */
  if (_expand_alias (op_str, &operand1, &operand2, &operand3))
    return;

  /* Encode and emit the instruction.  */
  _emit_instruction (op_str, &operand1, &operand2, &operand3);
}

const char *
md_atof (int type, char *litP, int *sizeP)
{
  return ieee_md_atof (type, litP, sizeP, FALSE);
}

int
md_parse_option (int c, const char *arg ATTRIBUTE_UNUSED)
{
  switch (c)
    {
    case OPTION_RELAX:
      s_v16_relax = TRUE;
      break;

    case OPTION_NO_RELAX:
      s_v16_relax = FALSE;
      break;

    default:
      return 0;
    }

  return 1;
}

void
md_show_usage (FILE *stream)
{
  fprintf (stream, _ ("\
V16 options:\n\
  -mrelax                 enable relax (default)\n\
  -mno-relax              disable relax\n\
"));
}

static void
_apply_insn_reloc_fix (fixS *fixP, char *buf, int32_t val, int bitsize,
		       int shift, int bitpos, bfd_boolean check_range)
{
  uint32_t word;

  /* Check ranges.  */
  if (check_range && ((val & ((1 << shift) - 1)) != 0))
    {
      as_bad_where (fixP->fx_file, fixP->fx_line, _ ("bad reloc alignment"));
      return;
    }
  val >>= shift;
  const int32_t lo = -(1 << (bitsize - 1));
  const int32_t hi = (1 << (bitsize - 1)) - 1;
  if (check_range && (val < lo || val > hi))
    {
      as_bad_where (fixP->fx_file, fixP->fx_line,
		    _ ("reloc out of range [%d,%d]: %d"), lo, hi, val);
      return;
    }
  val = (val & ((1 << bitsize) - 1)) << bitpos;

  /* Read and patch the instruction half-word.  */
  word = (uint32_t) bfd_getl16 (buf);
  word |= (uint32_t) val;
  bfd_putl16 ((bfd_vma) word, buf);
}

/* Apply a fixup to the object file.  */
void
md_apply_fix (fixS *fixP, valueT *valP, segT seg ATTRIBUTE_UNUSED)
{
  char *buf = fixP->fx_where + fixP->fx_frag->fr_literal;
  int32_t val = (int32_t) *valP;

  /* TODO(m): Many (all?) of these should be skipped if fx_done == 0 (see
     below).  */
  switch (fixP->fx_r_type)
    {
    case BFD_RELOC_32:
      md_number_to_chars (buf, *valP, 4);
      break;

    case BFD_RELOC_16:
      md_number_to_chars (buf, *valP, 2);
      break;

    case BFD_RELOC_8:
      md_number_to_chars (buf, *valP, 1);
      break;

    case BFD_RELOC_V16_PCREL_8X2:
      _apply_insn_reloc_fix (fixP, buf, val, 8, 1, 0, TRUE);
      break;

    case BFD_RELOC_V16_PCREL_8X4_SHL4:
      _apply_insn_reloc_fix (fixP, buf, val, 8, 2, 4, TRUE);
      break;

    case BFD_RELOC_V16_PCREL_HI8_SHL4:
      /* TODO(m): Check range overflow (but not alignment?).  */
      _apply_insn_reloc_fix (fixP, buf, val, 8, 22, 4, FALSE);
      break;

    case BFD_RELOC_V16_PCREL_MID8_SHL4:
      /* TODO(m): Check range overflow (but not alignment?).  */
      _apply_insn_reloc_fix (fixP, buf, val, 8, 12, 4, FALSE);
      break;

    case BFD_RELOC_V16_PCREL_HI10:
      /* TODO(m): Check range overflow (but not alignment?).  */
      _apply_insn_reloc_fix (fixP, buf, val, 10, 22, 0, FALSE);
      break;

    case BFD_RELOC_V16_PCREL_MID10:
      _apply_insn_reloc_fix (fixP, buf, val, 10, 12, 0, FALSE);
      break;

    case BFD_RELOC_V16_PCREL_LO10X4:
      _apply_insn_reloc_fix (fixP, buf, val, 10, 2, 0, FALSE);
      break;

    default:
      abort ();
    }

  if (fixP->fx_addsy == NULL && fixP->fx_pcrel == 0)
    fixP->fx_done = 1;
}

/* Put number into target byte order.  */
void
md_number_to_chars (char *ptr, valueT use, int nbytes)
{
  number_to_chars_littleendian (ptr, use, nbytes);
}

/* Generate a machine-dependent relocation.  */
arelent *
tc_gen_reloc (asection *section, fixS *fixP)
{
  arelent *reloc;

  reloc = XNEW (arelent);
  reloc->howto = bfd_reloc_type_lookup (stdoutput, fixP->fx_r_type);
  if (reloc->howto == NULL)
    {
      as_bad_where (fixP->fx_file, fixP->fx_line,
		    _ ("internal error: can't export reloc type %d (`%s')"),
		    fixP->fx_r_type, bfd_get_reloc_code_name (fixP->fx_r_type));
      return NULL;
    }

  gas_assert (!fixP->fx_pcrel == !reloc->howto->pc_relative);

  reloc->sym_ptr_ptr = XNEW (asymbol *);
  *reloc->sym_ptr_ptr = symbol_get_bfdsym (fixP->fx_addsy);
  reloc->address = fixP->fx_frag->fr_address + fixP->fx_where;

  if (fixP->fx_pcrel)
    {
      if (section->use_rela_p)
	fixP->fx_offset -= md_pcrel_from_section (fixP, section);
      else
	fixP->fx_offset = reloc->address;
    }
  reloc->addend = fixP->fx_offset;

  if (fixP->fx_r_type == BFD_RELOC_VTABLE_ENTRY)
    reloc->address = fixP->fx_offset;

  return reloc;
}

/* The location from which a PC relative jump should be calculated,
   given a PC relative reloc.  */
long
md_pcrel_from_section (fixS *fixP, segT sec)
{
  offsetT base = fixP->fx_where + fixP->fx_frag->fr_address;

  if (fixP->fx_addsy != (symbolS *) NULL &&
      (!S_IS_DEFINED (fixP->fx_addsy) ||
       (S_GET_SEGMENT (fixP->fx_addsy) != sec) ||
       S_IS_EXTERNAL (fixP->fx_addsy) || S_IS_WEAK (fixP->fx_addsy)))
    {
      /* The symbol is undefined (or is defined but not in this section).
	 Let the linker figure it out.  */
      return 0;
    }

  /* Some relocs use base alignment that is wider than the instruction size.  */
  switch (fixP->fx_r_type)
    {
    case BFD_RELOC_V16_PCREL_8X4_SHL4:
    case BFD_RELOC_V16_PCREL_LO10X4:
      base &= ~3;
      break;
    default:
      break;
    }

  return base;
}

/* Can't use symbol_new here, so have to create a symbol and then at
   a later date assign it a value. That's what these functions do.  */

static void
_symbol_locate (
    symbolS *symbolP,
    const char *name, /* It is copied, the caller can modify.	 */
    segT segment,     /* Segment identifier (SEG_<something>).  */
    valueT valu,      /* Symbol value.  */
    fragS *frag)      /* Associated fragment.	 */
{
  char *preserved_copy_of_name;
  const size_t name_length = strlen (name) + 1; /* +1 for \0.  */
  obstack_grow (&notes, name, name_length);
  preserved_copy_of_name = (char *) obstack_finish (&notes);

  S_SET_NAME (symbolP, preserved_copy_of_name);
  S_SET_SEGMENT (symbolP, segment);
  S_SET_VALUE (symbolP, valu);
  symbol_clear_list_pointers (symbolP);
  symbol_set_frag (symbolP, frag);

  /* Link to end of symbol chain.  */
  {
    extern int symbol_table_frozen;
    if (symbol_table_frozen)
      abort ();
  }
  symbol_append (symbolP, symbol_lastP, &symbol_rootP, &symbol_lastP);
  obj_symbol_new_hook (symbolP);

#ifdef DEBUG_SYMS
  verify_symbol_chain (symbol_rootP, symbol_lastP);
#endif /* DEBUG_SYMS  */
}

/* Commit the contents of the literal pool, if any, for this segment and
   sub-segment.  */

static void
_commit_literal_pool (void)
{
  unsigned int entry;
  literal_pool_t *pool;
  char sym_name[20];

  demand_empty_rest_of_line ();
  pool = _get_literal_pool (FALSE);
  if (pool == NULL || pool->symbol == NULL || pool->next_free_entry == 0)
    return;

  /* Align pool as you have word accesses.
     Only make a frag if we have to.  */
  if (!need_pass_2)
    frag_align (pool->alignment, 0, 0);
  record_alignment (now_seg, 2);

  sprintf (sym_name, "$$lit_\002%x", pool->id);

  _symbol_locate (pool->symbol, sym_name, now_seg, (valueT) frag_now_fix (),
		  frag_now);
  symbol_table_insert (pool->symbol);

  for (entry = 0; entry < pool->next_free_entry; entry++)
    {
#ifdef OBJ_ELF
      if (debug_type == DEBUG_DWARF2)
	dwarf2_gen_line_info (frag_now_fix (), pool->locs + entry);
#endif
      /* First output the expression in the instruction to the pool.  */
      emit_expr (&(pool->literals[entry]), 4);
    }

  /* Mark the pool as empty.  */
  pool->next_free_entry = 0;
  pool->symbol = NULL;
}

/* Finalization.  */

void
v16_cleanup (void)
{
  literal_pool_t *pool;

  for (pool = s_list_of_pools; pool; pool = pool->next)
    {
      /* Put it at the end of the relevant section.  */
      subseg_set (pool->section, pool->sub_section);
      _commit_literal_pool ();
    }
}

static const pseudo_typeS v16_pseudo_table[] = {
    {"half", cons, 2}, {"word", cons, 4}, {NULL, NULL, 0}};

void
v16_pop_insert (void)
{
  pop_insert (v16_pseudo_table);
  return;
}
