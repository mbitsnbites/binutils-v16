/* V16 elf support for BFD.
   Copyright (C) 2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard (m@bitsnbites.eu).

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#ifndef _ELF_V16_H
#define _ELF_V16_H

#include "elf/reloc-macros.h"

/* Relocations.  */
START_RELOC_NUMBERS(elf_v16_reloc_type)
  /* Relocation types used by the dynamic linker.  */
  RELOC_NUMBER (R_V16_NONE,    0)
  RELOC_NUMBER (R_V16_32,      1)

  /* Relocation types related to relaxation.  */
  RELOC_NUMBER (R_V16_RELAX,   2)

  /* Relocation types not used by the dynamic linker.  */
  RELOC_NUMBER (R_V16_PCREL_8X2, 3)
  RELOC_NUMBER (R_V16_PCREL_8X4_SHL4, 4)
  RELOC_NUMBER (R_V16_PCREL_HI8_SHL4, 5)
  RELOC_NUMBER (R_V16_PCREL_MID8_SHL4, 6)
  RELOC_NUMBER (R_V16_PCREL_HI10, 7)
  RELOC_NUMBER (R_V16_PCREL_MID10, 8)
  RELOC_NUMBER (R_V16_PCREL_LO10X4, 9)
END_RELOC_NUMBERS(R_V16_max)

#endif /* _ELF_V16_H */
