/* Definitions for decoding the V16 opcode table.
   Copyright (C) 2024 Free Software Foundation, Inc.
   Contributed by Marcus Geelnard (m@bitsnbites.eu).

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA
   02110-1301, USA.  */

#ifndef OPCODE_V16_H
#define OPCODE_V16_H

#include <stdbool.h>

/* Instruction formats.  */
enum v16_fmt
{
  V16_FMT_BAD = 0,
  V16_FMT_A,
  V16_FMT_B,
  V16_FMT_C,
  V16_FMT_D,
  V16_FMT_E,
  V16_FMT_F,
  V16_FMT_G,
  V16_FMT_H,
  V16_FMT_I,
};

enum v16_args
{
  V16_ARGS_NONE = 0,
  V16_ARGS_I,
  V16_ARGS_R,
  V16_ARGS_V_R,
  V16_ARGS_R_R,
  V16_ARGS_R_I,
  V16_ARGS_R_U, /* Same as V16_ARGS_R_I, but unsigned immediate.  */
  V16_ARGS_ADDR,
  V16_ARGS_R_R_I,
  V16_ARGS_R_SP_I,
  V16_ARGS_R_ADDR,
  V16_ARGS_R5_I,
  V16_ARGS_R5_ADDR,
};

typedef struct v16_opc_info_t
{
  const char *name;    /* Mnemonic.  */
  const char *key;     /* Hash map lookup key (mnemonic + args).  */
  unsigned short insn; /* Instruction word.  */
  enum v16_fmt format; /* Format type.  */
  enum v16_args args;  /* Argument type. */
  int shift;	       /* Left-shift for the immediate field.  */
  bool signext;	       /* Should the immediate field be sign extended?  */
} v16_opc_info_t;

extern const v16_opc_info_t v16_opc_type_a_info[32];
extern const v16_opc_info_t v16_opc_type_b_info[96];
extern const v16_opc_info_t v16_opc_type_c_info[32];
extern const v16_opc_info_t v16_opc_type_d_info[32];
extern const v16_opc_info_t v16_opc_type_e_info[12];
extern const v16_opc_info_t v16_opc_type_f_info[4];
extern const v16_opc_info_t v16_opc_type_g_info[6];
extern const v16_opc_info_t v16_opc_type_h_info[5];
extern const v16_opc_info_t v16_opc_type_i_info[4];

#endif /* OPCODE_V16_H */
